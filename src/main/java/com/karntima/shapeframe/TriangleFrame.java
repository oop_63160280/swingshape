/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.karntima.shapeframe;

import java.awt.Color;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JTextField;

/**
 *
 * @author User
 */
public class TriangleFrame {

    public static void main(String[] args) {
        final JFrame frame = new JFrame("Triangle");
        frame.setSize(500, 300);
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        frame.setLayout(null);

        JLabel lblBase = new JLabel("Base:", JLabel.TRAILING);
        lblBase.setSize(35, 20);
        lblBase.setLocation(5, 5);
        lblBase.setBackground(Color.WHITE);
        lblBase.setOpaque(true);
        frame.add(lblBase);

        final JTextField txtBase = new JTextField(); //
        txtBase.setSize(50, 20);
        txtBase.setLocation(45, 5);
        frame.add(txtBase);

        JLabel lblHeight = new JLabel("Height:", JLabel.TRAILING);
        lblHeight.setSize(50, 20);
        lblHeight.setLocation(100, 5);
        lblHeight.setBackground(Color.WHITE);
        lblHeight.setOpaque(true);
        frame.add(lblHeight);

        final JTextField txtHeight = new JTextField(); //
        txtHeight.setSize(50, 20);
        txtHeight.setLocation(155, 5);
        frame.add(txtHeight);

        JButton btnCalculate = new JButton("Calculate");
        btnCalculate.setSize(100, 20);
        btnCalculate.setLocation(220, 5);
        frame.add(btnCalculate);

        JLabel lblResult = new JLabel("Triangle Base = ???  Height = ??? Area = ??? Perimeter = ???");
        lblResult.setHorizontalAlignment(JLabel.CENTER);
        lblResult.setSize(400, 20);
        lblResult.setLocation(0, 50);
        lblResult.setBackground(Color.yellow);
        lblResult.setOpaque(true);
        frame.add(lblResult);

        btnCalculate.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                try {
                    String strBase = txtBase.getText();
                    String strHeight = txtHeight.getText();
                    double Base = Double.parseDouble(strBase);
                    double Height = Double.parseDouble(strHeight);
                    Triangle triangle = new Triangle(Base, Height);
                    lblResult.setText(String.format("Triangle B = %.2f", triangle.getBase())
                            + String.format(" H = %.2f", triangle.getHeight())
                            + String.format(" Area =  %.2f", triangle.calArea())
                            + " Perimeter = " + String.format("%.2f", triangle.calPerimeter()));
                } catch (Exception ex) {
                    JOptionPane.showMessageDialog(frame, "Error: pleasr input number",
                             "Error", JOptionPane.ERROR_MESSAGE);
                    txtBase.setText("");
                    txtBase.requestFocus();
                    txtHeight.setText("");
                    txtHeight.requestFocus();
                }
            }

        });

        frame.setVisible(true);
    }
}
