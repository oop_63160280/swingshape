/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.karntima.shapeframe;

import java.awt.Color;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JTextField;

/**
 *
 * @author User
 */
public class RectangleFrame {
    public static void main(String[] args) {
        final JFrame frame = new JFrame("Rectangle");
        frame.setSize(500, 300);
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        frame.setLayout(null);
        
        JLabel lblWide = new JLabel("Wide:",JLabel.TRAILING);
        lblWide.setSize(35, 20);
        lblWide.setLocation(5, 5);
        lblWide.setBackground(Color.WHITE);
        lblWide.setOpaque(true);
        frame.add(lblWide);
        
        final JTextField txtWide = new JTextField(); //
        txtWide.setSize(50, 20);
        txtWide.setLocation(45, 5);
        frame.add(txtWide);
        
        JLabel lblHeight = new JLabel("Height:",JLabel.TRAILING);
        lblHeight.setSize(50, 20);
        lblHeight.setLocation(100, 5);
        lblHeight.setBackground(Color.WHITE);
        lblHeight.setOpaque(true);
        frame.add(lblHeight);
        
        final JTextField txtHeight = new JTextField(); //
        txtHeight.setSize(50, 20);
        txtHeight.setLocation(155, 5);
        frame.add(txtHeight);
        
        JButton btnCalculate =new JButton("Calculate");
        btnCalculate.setSize(100, 20);
        btnCalculate.setLocation(220, 5);
        frame.add(btnCalculate);
        
        JLabel lblResult = new JLabel("Rectangle Wide = ???  Height = ??? Area = ??? Perimeter = ???");
        lblResult.setHorizontalAlignment(JLabel.CENTER);
        lblResult.setSize(400, 20);
        lblResult.setLocation(0, 50);
        lblResult.setBackground(Color.yellow);
        lblResult.setOpaque(true);
        frame.add(lblResult);
        
         btnCalculate.addActionListener(new ActionListener(){
            @Override
            public void actionPerformed(ActionEvent e) {
                try{
                    String strWide = txtWide.getText();
                    String strHeight = txtHeight.getText();
                    double Wide = Double.parseDouble(strWide);
                    double Height = Double.parseDouble(strHeight);
                    Rectangle rectangle = new Rectangle(Wide,Height);
                    lblResult.setText(String.format("Rectangle W = %.2f", rectangle.getWide())
                            + String.format(" H = %.2f", rectangle.getHeight())
                            + String.format(" area =  %.2f", rectangle.calArea()) 
                            + " perimeter = " + String.format("%.2f", rectangle.calPerimeter()));
                }catch(Exception ex){
                    JOptionPane.showMessageDialog(frame,"Error: pleasr input number"
                            ,"Error",JOptionPane.ERROR_MESSAGE);
                    txtWide.setText("");
                    txtWide.requestFocus();
                    txtHeight.setText("");
                    txtHeight.requestFocus();
                }
            }
             
         });
        
        
        frame.setVisible(true);
        
    }
}
