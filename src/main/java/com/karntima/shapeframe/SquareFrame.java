/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.karntima.shapeframe;

import java.awt.Color;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JTextField;

/**
 *
 * @author User
 */
public class SquareFrame {
    public static void main(String[] args) {
        final JFrame frame = new JFrame("Squre");
        frame.setSize(500, 300);
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        frame.setLayout(null);
        
        JLabel lblSide = new JLabel("Side:",JLabel.TRAILING);
        lblSide.setSize(35, 20);
        lblSide.setLocation(5, 5);
        lblSide.setBackground(Color.WHITE);
        lblSide.setOpaque(true);
        frame.add(lblSide);
        
        final JTextField txtSide = new JTextField();
        txtSide.setSize(60, 20);
        txtSide.setLocation(45, 5);
        frame.add(txtSide);
        
        JButton btnCalculate =new JButton("Calculate");
        btnCalculate.setSize(100, 20);
        btnCalculate.setLocation(120, 5);
        frame.add(btnCalculate);
        
        JLabel lblResult = new JLabel("Square Side = ??? Area = ??? Perimeter = ???");
        lblResult.setHorizontalAlignment(JLabel.CENTER);
        lblResult.setSize(400, 20);
        lblResult.setLocation(0, 50);
        lblResult.setBackground(Color.yellow);
        lblResult.setOpaque(true);
        frame.add(lblResult);
        
        btnCalculate.addActionListener(new ActionListener(){
            @Override
            public void actionPerformed(ActionEvent e) {
                try{
                    String strSide = txtSide.getText();
                    double Side = Double.parseDouble(strSide);
                    Square square = new Square(Side);
                    lblResult.setText(String.format("Square S = %.2f", square.getSide())
                            + String.format(" Area =  %.2f", square.calArea()) 
                            + " Perimeter = " + String.format("%.2f", square.calPerimeter()));
                }catch(Exception ex){
                    JOptionPane.showMessageDialog(frame,"Error: pleasr input number"
                            ,"Error",JOptionPane.ERROR_MESSAGE);
                    txtSide.setText("");
                    txtSide.requestFocus();
                }
            }
             
         });
        
        
        frame.setVisible(true);
    }
}
