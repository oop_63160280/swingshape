/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.karntima.shapeframe;

import java.awt.Color;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JTabbedPane;
import javax.swing.JTextField;

/**
 *
 * @author User
 */
public class CircleFrame {
    public static void main(String[] args) {
        final JFrame frame = new JFrame("Circle");
        frame.setSize(500, 300);
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        frame.setLayout(null);
        
        JLabel lblRadius = new JLabel("radius:",JLabel.TRAILING);
        lblRadius.setSize(45, 20);
        lblRadius.setLocation(5, 5);
        lblRadius.setBackground(Color.WHITE);
        lblRadius.setOpaque(true);
        frame.add(lblRadius);
        
        final JTextField txtRadius = new JTextField();
        txtRadius.setSize(60, 20);
        txtRadius.setLocation(55, 5);
        frame.add(txtRadius);
       
        JButton btnCalculate =new JButton("Calculate");
        btnCalculate.setSize(100, 20);
        btnCalculate.setLocation(120, 5);
        frame.add(btnCalculate);
        
        final JLabel lblResult = new JLabel("Circle radius = ??? area = ??? perimeter = ???");
        lblResult.setHorizontalAlignment(JLabel.CENTER);
        lblResult.setSize(400, 20);
        lblResult.setLocation(0, 50);
        lblResult.setBackground(Color.yellow);
        lblResult.setOpaque(true);
        frame.add(lblResult);
        
        //Annoymous class
        //Even Driven
        btnCalculate.addActionListener(new ActionListener(){
            @Override
            public void actionPerformed(ActionEvent e) {
            //    System.out.println("Click");   //ex.show when click
                try{
                    // 1.puch information fortxtRadius -> StrRadius
                    String strRadius = txtRadius.getText();
                    // 2.chang strRadius -> radius: double parseDouble
                    double radius = Double.parseDouble(strRadius); // -> throw NumberFomatException
                    // 3.instance object Circle(radius) -> circle
                    Circle circle = new Circle(radius);
                    //Circle circle = null;
                    // 4.update lblResult (show full)
                    lblResult.setText(String.format("Circle r = %.2f", circle.getRadius())
                            +" area = " + String.format("%.2f", circle.calArea()) 
                            + " perimeter = " + String.format("%.2f", circle.calPerimeter()));
                }
                //catch(NumberFormatException ex){
                    // do the row63
                //    System.out.println("Error!!! Number Format Exception");
                //}catch(NullPointerException ex){
                //    System.out.println("Error!!! Null Pointer Exception");
                //}
                catch(Exception ex){
                //    System.out.println("Error!!! Another Exception");
                    JOptionPane.showMessageDialog(frame,"Error: pleasr input number"
                            ,"Error",JOptionPane.ERROR_MESSAGE);
                    txtRadius.setText("");
                    txtRadius.requestFocus();
                }
            }
            
        });
        
        
        
        frame.setVisible(true);
        
    }
}
